/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package athletewithdatabase;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author echom_000
 */
public class AthleteDbManager {
        private final String jdbcDriver;
        private final String dbUrl;
   
        private String dbUser;
        private String dbPass;
        
        private final String schemaName;
        
        private final Map<String, String> tableList;
        
        public AthleteDbManager(String driver, String url, String user, String pass, String schema) {
            jdbcDriver = driver;
            dbUrl = url;
            dbUser = user;
            dbPass = pass;
            schemaName = schema;
            
            tableList = new HashMap<>();
        }
        
        
        public boolean addAthlete(String athleteName, double athleteSalary) {
            String prepStr = "INSERT INTO " + getTableName("athletes") + " (NAME, SALARY)"
                    + " VALUES (?, ?)";
            try (Connection conn = getConnection();
                 PreparedStatement stmt = conn.prepareStatement(prepStr);) {
                stmt.setString(0, athleteName);
                stmt.setDouble(1, athleteSalary);
                
                stmt.execute();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "She done threw a rod Laszlo", "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                return false;
            }
            return true;
        }
        
        //This drops every athlete with that name but oh well
        public boolean dropAthleteByName(String athleteName) {
            String prepStr = "DELETE FROM " + getTableName("athletes")
                    + " WHERE NAME=?";
            
            try (Connection conn = getConnection();
                    PreparedStatement stmt = conn.prepareStatement(prepStr);) {
                
                stmt.setString(0, athleteName);
                stmt.execute();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "She done threw a rod Laszlo", "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                return false;
            }
            
            return true;
        }
        
        public boolean addEmployee(String athleteName, String employeeName, String employeeType) {
            //Retrieve the athlete's id with getAthleteIdByName, then insert the employee into the table
            //with the athlete id and the employee type as foreign keys.
            return true;
        }
        
        public int getAthleteIdByName(String athleteName) {
            //Select the ID of the first athlete we find by name. We should get the inserted ID with metadata and
            //hold on to it but there's no time now!
            return 0;
        }
        
        public double getEmployeeSalary(String employeeName) {
            //Get the employee's salary by joining on the employee type table. Slow but I wanted to have a join!
            //I didn't even get to write it!
            return 0.0;
        }
        
        public String getSchema() {
            return schemaName;
        }
        
        public void addTableName(String key, String val) {
            tableList.put(key, val);
        }
        
        public String getTableName(String key) {
            return tableList.get(key);
        }
        
        public String getJdbcDriver() {
            return jdbcDriver;
        }

        public String getDbUrl() {
            return dbUrl;
        }

        public String getDbUser() {
            return dbUser;
        }

        public String getDbPass() {
            return dbPass;
        }

        public void setDbUser(String dbUser) {
            this.dbUser = dbUser;
        }

        public void setDbPass(String dbPass) {
            this.dbPass = dbPass;
        }
        
        public Connection getConnection() throws SQLException {
            return DriverManager.getConnection(dbUrl, dbUser, dbPass);
        }
        
        //Everything here is probably not broken anymore
    public boolean bootstrapDb() {
        
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Unable to load database driver.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return false;
        }
        
        try (Connection conn = getConnection()) {
            createTableIfNotExists(conn, "athletes", " ( ATHLETE_ID INT GENERATED ALWAYS AS IDENTITY CONSTRAINT ATHLETE_PK PRIMARY KEY, "
                                                                    + "NAME VARCHAR(255) NOT NULL, SALARY DOUBLE )");
            createTableIfNotExists(conn, "empTypes", " ( EMPLOYEE_TYPE VARCHAR(50) NOT NULL CONSTRAINT EMPTYPE_PK PRIMARY KEY, SALARY_MULTIPLIER DOUBLE NOT NULL )");
            createTableIfNotExists(conn, "employees", " ( EMPLOYEE_ID INT GENERATED ALWAYS AS IDENTITY CONSTRAINT EMPLOYEE_PK PRIMARY KEY, "
                                                                    + "NAME VARCHAR(255) NOT NULL, SALARY DOUBLE NOT NULL, "
                                                                    + "EMPLOYEE_TYPE VARCHAR(50) NOT NULL CONSTRAINT EMPTYPE_FK REFERENCES " + getTableName("empTypes") + " ON UPDATE RESTRICT, "
                                                                    + "ATHLETE_ID INT NOT NULL CONSTRAINT ATHLETE_FK REFERENCES " + getTableName("athletes") + " ON DELETE CASCADE )");
            
            
            
            try (Statement stmt = conn.createStatement()) {
                ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS count FROM " + getTableName("empTypes"));
                rs.next();
                
                if (rs.getInt("count") == 0) {
                    //Set up the employee wage table
                    stmt.executeUpdate("INSERT INTO " + getTableName("empTypes") + " VALUES ('Personal Assistant', 0.03), ('Lawyer', 0.1), ('Trainer', 0.05), ('Agent', 0.07)");
                }
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Database error. Check your username and password.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    //I should have this take a data structure for the columns but I'm just gonna take a SQL string and assume it's the right format
    private void createTableIfNotExists(Connection conn, String tableName, String columnString) {
        //http://somesimplethings.blogspot.com/2010/03/derby-create-table-if-not-exists.html
            //Get metadata so we can check if the tables exist
            DatabaseMetaData dbmd = null;
            try
            {
                dbmd= conn.getMetaData();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Database connection error.", "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
            
            String tbl = getTableName(tableName);
            //if the users table doesn't exist, create it and add a default user
            if (dbmd != null) {
                try (ResultSet rs = dbmd.getTables(null, schemaName, tbl, null);
                    Statement stmt = conn.createStatement()) {
                if (!rs.next()) {
                    stmt.executeUpdate("CREATE TABLE " + tbl + columnString);
                }
                } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Database connection error.", "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                }
            }
        }
    }
